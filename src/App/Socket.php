<?php
namespace App;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use App\Controller\Home as Controller;

class Socket implements MessageComponentInterface {
    protected $clients;
    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }
    public function onOpen(ConnectionInterface $conn) {
        $this->clients->attach($conn);
        echo "Client ({$conn->resourceId}) acabou de entrar! >> Total: " . count($this->clients) . " clientes\n";
    }
    public function onMessage(ConnectionInterface $from, $msg) {
        $msg = $this->getMensagem($msg);
        if ($msg) {
            $metodo = $msg->metodo;
            $from->send((new Controller)->$metodo($msg->parametros));
        }
    }
    public function onClose(ConnectionInterface $conn) {
        $this->clients->detach($conn);
        echo "Client ({$conn->resourceId}) acabou de sair! >> Total: " . count($this->clients) . " clientes\n";
    }
    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }
    private function getMensagem($msg)
    {
        $msg = json_decode($msg);
        if (isset($msg->metodo) && isset($msg->parametros)) {
            return $msg;
        }

        return false;
    }
}
