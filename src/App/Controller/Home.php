<?php
namespace App\Controller;

use App\Service\Usuario;

class Home {
	public function identificar($rm)
	{
		return (new Usuario)->getUsuario($rm)->getRm();
	}
}