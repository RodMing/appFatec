<?php
namespace App\Service;

use \App\Entity\Usuario as UsuarioEntity;

class Usuario {
	public function getUsuario($rm)
	{
		return (new UsuarioEntity)->setRm($rm);
	}
}
